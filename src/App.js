import React, { Component } from 'react'
import Axios from 'axios';
import MovieModal from './layouts/movieModal';

class App extends Component {
    state = {
        isLoading:true,
        invoices:[
            {
                "Movieid": "100",
                "Name":"telmex",
                "Studios":"19",
                "Stock":"19",
                "Price":"19", 
                "Year":"08/21/2020"
            }
        ],
        invoice:{
            "Movieid": null,
            "Name": null,
            "Studios": null,
            "Stock": null,
            "Price": null, 
            "Year": null
        },
        isPush:false
    };

    constructor(props) {
        super(props);
        this.changeHandler = this.changeHandler.bind(this);
        this.submitHandler = this.submitHandler.bind(this);
    } 

    submitHandler(){
        if(this.state.isPush){
            this.postPelicula();
        }else{
            this.putPelicula();
        }
    }

    editMovie(movie,flag){
        const moviejs = JSON.parse(JSON.stringify(movie));
        this.setState({invoice:moviejs,isPush:flag});
    }

    changeHandler(e) {
        let newData = this.state.invoice;
        newData[e.target.name] = e.target.value;
        this.setState({invoice:newData});
    }

    postPelicula(){
        let movie = this.state.invoice;
        movie.Movieid = this.state.invoices[this.state.invoices.length-1].Movieid+1;
        console.log(movie);
        Axios.post('https://2n5ewf6y8h.execute-api.us-east-2.amazonaws.com/dev/',movie)
            .then(response =>{

            });
    }

    putPelicula(){
        const movie = this.state.invoice;
        Axios.put('https://2n5ewf6y8h.execute-api.us-east-2.amazonaws.com/dev/'+movie.Movieid,movie)
            .then(response => {

            });
    }
    
    deletePelicula(Movieid){
        Axios.delete('https://2n5ewf6y8h.execute-api.us-east-2.amazonaws.com/dev/'+Movieid)
            .then(response => {
                console.log(response.data);
        });
        const updateedInvoices=[...this.state.invoices].filter(i=>i.Movieid !== Movieid);
        this.setState({invoices:updateedInvoices});
    }

    componentDidMount(){
        Axios.get('https://2n5ewf6y8h.execute-api.us-east-2.amazonaws.com/dev').then(response => {
            if(response.status === 200){
                const awsResponse = response.data.body.Items;
                this.setState({
                    invoices:awsResponse,
                    isLoading:false
                });
            }    
        });
    }

    render() { 

        const isLoading=this.state.isLoading;
        const allinvoices = this.state.invoices;
        const invoiceBase = {
            "Movieid": null,
            "Name": null,
            "Studios": null,
            "Stock": null,
            "Price": null, 
            "Year": null
        };
        
        if(isLoading)
            return(<div>Loading</div>);

        let invoices=
            allinvoices.map( (invoice) =>
                <tr key={invoice.Movieid}>
                    <td>{invoice.Name}</td>
                    <td>{invoice.Studios}</td> 
                    <td>{invoice.Stock}</td>
                    <td>{invoice.Price}</td>
                    <td>{invoice.Year}</td>
                <td><button onClick={()=> this.editMovie(invoice,false)} type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#staticBackdrop">Editar</button></td>
                <td><button className="btn btn-outline-danger" onClick={()=> this.deletePelicula(invoice.Movieid)}>Borrar</button></td>
                </tr>
            );
        
        return (
            <div>
                <MovieModal 
                        data={this.state.invoice}
                        changeHandler = {this.changeHandler}
                        submitHandler = {this.submitHandler}
                    ></MovieModal>
                <div className="container">
                    <div className="row-5"> 
                       
                        <div className=".col-10-xs-10 center test-center">        
                            <table class="table table-dark">
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="row">Nombre</th>
                                        <th>Estudio</th>
                                        <th>Cantidad</th>
                                        <th>Precio Unitario</th>
                                        <th>Año publicacion</th>
                                        <th colSpan="4">Acciones </th>
                                        <th >Imagen </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.invoices.length ===0 ? <td colSpan="9">All caught up! </td>: invoices}
                                </tbody>
                            </table>
                            <div >
                            <button onClick={()=> this.editMovie(invoiceBase,true)} type="button" class="btn btn-success" data-toggle="modal" data-target="#staticBackdrop">Agregar</button>
                            </div>
                           
                        </div>
                    </div>
                </div>
            </div>
   );
    }
}
 
export default App;