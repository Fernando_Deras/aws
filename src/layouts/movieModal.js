import React from 'react';
export default function MovieModal(props){
    return(
        <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Modal Movie</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div>
                    <div className="col">
                    <div  className="form-group row">
                        <label htmlFor="inputName" className="col-sm-2 col-form-label">Name</label>
                        <div className="col-sm-10">
                        <input type="text" 
                            className="form-control" 
                            id="inputName" name="Name" 
                            placeholder="First Name" 
                            value={props.data.Name || ''}
                            onChange={props.changeHandler} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="inputStudios"className="col-sm-2 col-form-label">Studios</label>
                        <div className="col-sm-10">
                        <input type="text"
                            className="form-control" 
                            id="inputStudios" 
                            name="Studios" 
                            placeholder="Studios" 
                            value={props.data.Studios || ''}
                            onChange={props.changeHandler} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="inputStock" className="col-sm-2 col-form-label">Stock</label>
                        <div className="col-sm-10">
                        <input type="number" 
                            className="form-control" 
                            id="inputStock" 
                            name="Stock" 
                            placeholder="Stock" 
                            value={props.data.Stock || ''}
                            onChange={props.changeHandler} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="inputPrice" className="col-sm-2 col-form-label">Price</label>
                        <div className="col-sm-10">
                            <input type="number" 
                                className="form-control" 
                                id="inputPrice" 
                                name="Price" 
                                value={props.data.Price || ''} 
                                onChange={props.changeHandler} 
                                placeholder="Price"/>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="inputYear" className="col-sm-2 col-form-label">Year</label>
                        <div className="col-sm-1">
                            <input type="date" id="start" name="Year"
                                value={props.data.Year || ''}
                                onChange={props.changeHandler} />
                        </div>
                    </div>
                    
                </div>
      </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onClick={props.submitHandler}>Confirmar</button>
      </div>
  </div>
</div>
</div>
    );
}
